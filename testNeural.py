from lasagne import layers
from lasagne.updates import nesterov_momentum, sgd,adam
from nolearn.lasagne import NeuralNet
from theano import tensor as T
from sklearn import mixture
from lasagne.nonlinearities import softmax
from sklearn.decomposition import PCA
from scipy.interpolate import interp1d
import functional

import numpy as np
import theano
import matplotlib.pyplot as plt
import os.path
import scipy.fftpack
import sys


from functional import findOptimalK
from functional import plotBicCurve
from functional import makePrediction

def load(path, testSize = 77565):
	data = []
	allLabels = []
	for path in path:
		content = [line.rstrip('\n').split('\t') for line in open(path)]
		label = [line[:1] for line in content]
		label = list(map(lambda x:x[0], label))
		content = [[int(el) for el in line[1:]] for line in content]
		data += content
		allLabels += label

	mapLabel = {}
	i = 0
	numericLabels = []
	for el in allLabels:
		if el not in mapLabel:
			mapLabel[el] = i
			numericLabels.append(i)
			i += 1
		else:
			numericLabels.append(mapLabel[el])

	test = np.array(data[testSize:])
	testLabels = np.array(numericLabels[testSize:]).astype(dtype= np.int32)

	train = np.array(data[:testSize])
	allLabels = np.array(numericLabels[:testSize]).astype(dtype= np.int32)
	
	test = test.astype(dtype = np.float32)
	train = train.astype(dtype = np.float32)

	allLabels = np.array(allLabels)
	testLabels = np.array(testLabels)

	return train, test, allLabels, testLabels


min_err = 0.1
def onEpochEnd(x, y):
	global min_err
	global test
	global testLabels
	global ind
	global hidden_size

	predicion = x.predict(test)
	count = 0
	for el,el2 in zip(predicion, testLabels):
		if el == el2:
			count += 1

	acc = float(count)/len(test)
	print('test acc = ' +  str(acc))
	if acc > min_err:
		min_err = acc
		x.save_params_to('neural' + str(hidden_size) + '.npz')

if __name__ == '__main__':
	hidden_size = int(sys.argv[1])
	autoEncoder = functional.initNeural(hidden_size)

	testPathes = ['../Features-A/NP1000.A.txt']
	pathes = ['../Features-A/NF100.A.txt', '../Features-A/NP125.A.txt', '../Features-A/NP250.A.txt', '../Features-A/NP500.A.txt']

	allData, test, allLabels, testLabels = load(pathes + testPathes)	
	autoEncoder.load_params_from('neural' + str(hidden_size) + '.npz')

	input_layer = autoEncoder.layers_['input'] 
	hidden_layer = autoEncoder.layers_['hidden']
	
	y = layers.get_output(hidden_layer)
	f = theano.function([input_layer.input_var], y)

	allData = f(allData)
	test = f(test)

	mapLabels = {}
	for el, vector in zip(allLabels, allData):
		if el in mapLabels:
			mapLabels[el].append(vector)
		else:
			mapLabels[el] = [vector]

	optimal_k_map = findOptimalK(mapLabels)
	makePrediction(mapLabels, testData, featureNumber)
