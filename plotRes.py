import matplotlib.pyplot as plt

acc = [0.951, 0.95, 0.946, 0.94, 0.937, 0.934, 0.931, 0.929, 0.924, 0.917, 0.89]
featuresNumber = [269, 250, 225,200,175,150,125,100,75,50,25]

plt.plot(featuresNumber, acc)
plt.ylabel('Accuracy')
plt.xlabel('Feature number')

axes = plt.gca()
plt.show()
