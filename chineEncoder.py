from lasagne import layers
from lasagne.updates import nesterov_momentum, sgd, adam
from nolearn.lasagne import NeuralNet
from sklearn.decomposition import PCA
import numpy as np
import theano
import sys
import functional

def load(path):
	data = []
	print('loading...')
	for p in path:
		content = [line.rstrip('\n').split('\t') for line in open(p)]
		content = [[int(el) for el in line[3:]] for line in content]
		data += content
	print('done.')
	return np.array(data).astype(dtype = np.float32)

i = 0
def onEpochEnd(x, y):
	global min_err
	global test
	global ind_min_err
	global i

	i += 1
	if i % 100 == 0:
		x.save_params_to('autoEncoderChineModel' + '.npz')


if __name__ == '__main__':
	hidden_size = int(sys.argv[1])
	chineAutoencoder = functional.initChineAutoencoder(hidden_size)

	pathes = ['FeatureChineese/train.txt']
	testPathes = ['FeatureChineese/verify.txt']

	train = load(pathes)
	chineAutoencoder.fit(train, train)
