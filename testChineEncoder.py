import numpy as np
import matplotlib.pyplot as plt
import os.path
import theano
import scipy.fftpack
import time
import sys
import functional

from sklearn import mixture
from sklearn.decomposition import PCA, IncrementalPCA
from scipy.interpolate import interp1d
from lasagne import layers
from lasagne.updates import nesterov_momentum, sgd, adam
from nolearn.lasagne import NeuralNet
from multiprocessing import Pool
from functools import partial


def updateRes(res, best, label):
	if best == -1:
		return np.array([[el, label] for el in res])
	else:
		for el, r in zip(best, res):
			if float(r) > float(el[0]):
				el[0] = r
				el[1] = label	
		return best

i = 0
def onEpochEnd(x, y):
	global min_err
	global test
	global ind_min_err
	global i
	global modelPath

	i += 1
	if i % 100 == 0:
		x.save_params_to(modelPath)

def predict(g, best, testData, label):
	start = time.time()
	res = g.score(testData)
	print('predicted time', time.time() - start)
	start = time.time()
	best = updateRes(res, best, label)
	print('up time', time.time() - start)
	return best
	
def trainGMM(symbolData, f, labelInd):
	global optimals

	symbolData = np.array(symbolData).astype(dtype=np.float32)
	symbolData = f(symbolData)
	optimalValue = int(optimals[labelInd][1])
	labelInd += 1

	g = mixture.GMM(n_components = optimalValue)
	g.fit(symbolData)

	best = predict(g, best, test, prevLabel)

	return best


if __name__ == '__main__':
	finalAcc = []
	averageK = []
	
	testPath = 'FeatureChineese/testPart.txt'
	trainPath = 'FeatureChineese/train.txt'
	modelPath = 'autoEncoderChineModel.npz'

	hidden_size = int(sys.argv[1])

	chineEncoder = functional.initChineAutoencoder(hidden_size)
	chineEncoder.load_params_from(modelPath)

	input_layer = chineEncoder.layers_['input'] 
	hidden_layer = chineEncoder.layers_['hidden']
	
	y = layers.get_output(hidden_layer)
	f = theano.function([input_layer.input_var], y)
	
	optimals = functional.loadOptimalK('index' + str(hidden_size) + '.txt')

	symbolData = []
	prevLabel = 'j_SHI'

	test = functional.loadTestData(testPath)
	test = f(test)

	labelInd = 0 
	best = -1
	for line in open(trainPath):
		line = line.rstrip('\n').split('\t')
	
		data = [int(el) for el in line[3:]]
		label = line[0]
		if label == prevLabel:
			symbolData.append(data)
		else:
			print('processing label ', label)
			best = trainGMM(symbolData, f, labelInd)
			labelInd += 1	
			prevLabel = label
			symbolData = []

	best = trainGMM(symbolData, f, labelInd)

	with open('prediction' + str(hidden_size) + '.txt', 'w') as out:
		for el in best:
			out.write(el[1] + '\n')
