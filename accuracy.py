import sys

featureNumb = sys.argv[1]

prediction = 'prediction' + featureNumb + '.txt'
real = 'FeatureChineese/testPart.txt'

realAns = []
for line in open(real):
	realAns.append(line.rstrip('\n').split('\t')[0])


print('In process ', prediction)
predAns = []

for line in open(prediction):
	predAns.append(line.rstrip('\n'))

count = 0
for el, el2 in zip(realAns, predAns):
	if el == el2:
		count += 1
print('Accuracy For ' + prediction + ': ' + str(float(count) / len(predAns))) 
