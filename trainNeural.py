#!/usr/bin/env python
# -*- coding: utf-8 -*-

from lasagne import layers
from lasagne.updates import nesterov_momentum, sgd, adam
from nolearn.lasagne import NeuralNet
from lasagne.nonlinearities import softmax
from sklearn.decomposition import PCA

import numpy as np
import theano
import random
import sys
import functional

def load(path, testSize = 77565):
	data = []
	allLabels = []
	for p in path:
		content = [line.rstrip('\n').split('\t') for line in open(p)]
		label = [line[:1] for line in content]
		label = list(map(lambda x:x[0], label))
		content = [[int(el) for el in line[1:]] for line in content]
		data += content
		allLabels += label

	mapLabel = {}
	i = 0
	numericLabels = []
	for el in allLabels:
		if el not in mapLabel:
			mapLabel[el] = i
			numericLabels.append(i)
			i += 1
		else:
			numericLabels.append(mapLabel[el])

	test = np.array(data[testSize:])
	testLabels = np.array(numericLabels[testSize:]).astype(dtype= np.int32)

	train = np.array(data[:testSize])
	allLabels = np.array(numericLabels[:testSize]).astype(dtype= np.int32)
	
	test = test.astype(dtype = np.float32)
	train = train.astype(dtype = np.float32)

	allLabels = np.array(allLabels)
	testLabels = np.array(testLabels)

	return train, test, allLabels, testLabels


min_err = 0.1
def onEpochEnd(x, y):
	global min_err
	global ind
	global test
	global testLabels
	global hidden_size

	predicion = x.predict(test)
	count = 0
	for el,el2 in zip(predicion, testLabels):
		if el == el2:
			count += 1

	acc = float(count)/len(test)
	print('test acc = ' +  str(acc))
	if acc > min_err:
		min_err = acc
		x.save_params_to('neural' + str(hidden_size) + '.npz')


if __name__ == '__main__':
	testPathes = ['Features-A/NP1000.A.txt']
	pathes = ['Features-A/NF100.A.txt', 'Features-A/NP125.A.txt', 'Features-A/NP250.A.txt', 'Features-A/NP500.A.txt']

	train, test, allLabels, testLabels = load(pathes + testPathes)
	hidden_size = int(sys.argv[1])

	neural = functional.initNeural(hidden_size)
	neural.fit(train, allLabels)
