import numpy as np
from sklearn import mixture

import matplotlib.pyplot as plt
import os.path
from sklearn.decomposition import PCA, IncrementalPCA
from scipy.interpolate import interp1d

import scipy.fftpack
from multiprocessing import Pool
import time
from functools import partial
import functional

def calcBic(symbolData, k):
	#global symbolData
	g = mixture.GMM(n_components = k)
			
	g.fit(symbolData)
	bic = g.bic(symbolData)
	return bic
	
def findOptK(symbolData, rangeOfK, prevLabel, pca):
	symbolData = np.array(symbolData)
	symbolData = pca.transform(symbolData)
	func = partial(calcBic, symbolData)

	bics = p.map(func, rangeOfK)

	opt = rangeOfK[np.argmin(bics)]
	print('opt k: ', opt)

	with open('index' + str(hidden_size) + '.txt', 'a') as out:
		out.write(prevLabel + '\t' + str(opt) + '\n')
	

if __name__ == '__main__':
	finalAcc = []
	averageK = []
	
	testPathes = ['../FeatureChineese/verify.txt']
	pathes = ['../FeatureChineese/train.txt']

	hidden_size = int(sys.argv[1])
	
	pca = functional.incrementalPCA(pathes)

	symbolData = []
	prevLabel = 'j_SHI'

	p = Pool(2)	
	optimalK = {}
	print('processing J_SHI...')
	start = time.time()
	
	for line in open(pathes[0]):
		line = line.rstrip('\n').split('\t')
	
		data = [int(el) for el in line[3:]]
		label = line[0]
		if label == prevLabel:
			symbolData.append(data)
		else:
			if len(symbolData) > 20:
				rangeOfK = range(1, 20)
			else:
				rangeOfK = range(1, len(symbolData))
			findOptK(symbolData, rangeOfK, prevLabel, pca)
			prevLabel = label
			symbolData = []
			
	findOptK(symbolData, rangeOfK, prevLabel, pca)