#!/usr/bin/env python
# -*- coding: utf-8 -*-
import theano
import numpy as np
import matplotlib.pyplot as plt
import os.path
import scipy.fftpack
import sys
import functional

from lasagne import layers
from lasagne.updates import nesterov_momentum, sgd,adam
from nolearn.lasagne import NeuralNet
from sklearn import mixture
from theano import tensor as T
from sklearn.decomposition import PCA
from scipy.interpolate import interp1d


from functional import findOptimalK
from functional import plotBicCurve
from functional import makePrediction

min_err = 100
def onEpochEnd(x, y):
	global min_err
	global test

	predicion = x.predict(test)
	mse = ((predicion - test) ** 2).mean(axis=None)

	print('test error = ' +  str(mse))
	if float(mse) < min_err:
		min_err = mse
		x.save_params_to('autoEncoderModel' + '.npz')

def load(path, testSize = 77565):
	data = []
	allLabels = []
	for path in path:
		content = [line.rstrip('\n').split('\t') for line in open(path)]
		label = [line[:1] for line in content]
		label = list(map(lambda x:x[0], label))
		content = [[int(el) for el in line[1:]] for line in content]
		allLabels += label
		data += content

	test = np.array(data[testSize:])
	testLabels = allLabels[testSize:]
	train = np.array(data[:testSize])
	allLabels = allLabels[:testSize]
	test = test.astype(dtype = np.float32)
	train = train.astype(dtype = np.float32)

	return train, test, allLabels, testLabels


if __name__ == '__main__':
	hidden_size = int(sys.argv[1])
	autoEncoder = functional.initAutoEncoder(hidden_size)

	testPathes = ['Features-A/NP1000.A.txt']
	pathes = ['Features-A/NF100.A.txt', 'Features-A/NP125.A.txt', 'Features-A/NP250.A.txt', 'Features-A/NP500.A.txt']

	allData, test, allLabels, testLabels = load(pathes + testPathes)	
	autoEncoder.load_params_from('autoEncoder' + str(hidden_size) + '.npz')

	input_layer = autoEncoder.layers_['input'] 
	hidden_layer = autoEncoder.layers_['hidden']
	
	y = layers.get_output(hidden_layer)
	f = theano.function([input_layer.input_var], y)

	allData = f(allData)
	test = f(test)

	mapLabels = {}
	for el, vector in zip(allLabels, allData):
		if el in mapLabels:
			mapLabels[el].append(vector)
		else:
			mapLabels[el] = [vector]


	optimal_k_map = findOptimalK(mapLabels)
	makePrediction(mapLabels, testData, hidden_size)
