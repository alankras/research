#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import theano
import sys

from lasagne import layers
from lasagne.updates import nesterov_momentum, sgd, adam
from nolearn.lasagne import NeuralNet
from sklearn.decomposition import PCA
import functional

def load(path, testSize = 77565):
	data = []
	for path in path:
		content = [line.rstrip('\n').split('\t') for line in open(path)]
		content = [[int(el) for el in line[1:]] for line in content]
		data += content

	test = np.array(data[testSize:])
	train = np.array(data[:testSize])
	test = test.astype(dtype = np.float32)
	train = train.astype(dtype = np.float32)

	return train, test

min_err = 100

def onEpochEnd(x, y):
	global min_err
	global test
	global train
	global hidden_size

	predicion = x.predict(test)
	pred2 = x.predict(train)
	mse2 = ((pred2 - train) ** 2).mean(axis=None)
	mse = ((predicion - test) ** 2).mean(axis=None)

	print('test error = ' +  str(mse))
	if float(mse) < min_err:
		min_err = mse
		x.save_params_to('autoEncoder' + str(hidden_size) + '.npz')	

if __name__ == '__main__':
	hidden_size = int(sys.argv[1])
	autoEncoder = functional.initAutoEncoder(hidden_size)

	testPathes = ['Features-A/NP1000.A.txt']
	pathes = ['Features-A/NF100.A.txt', 'Features-A/NP125.A.txt', 'Features-A/NP250.A.txt', 'Features-A/NP500.A.txt']

	train, test = load(pathes + testPathes)
	#train, test = functional.normalize(train, test)

	autoEncoder.fit(train, train)
