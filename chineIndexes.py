import numpy as np
from sklearn import mixture

import matplotlib.pyplot as plt
import os.path
from sklearn.decomposition import PCA, IncrementalPCA
from scipy.interpolate import interp1d

import scipy.fftpack
from multiprocessing import Pool
import time
from functools import partial
from lasagne import layers
from lasagne.updates import nesterov_momentum, sgd, adam
from nolearn.lasagne import NeuralNet
import theano
import sys
import functional

def calcBic(symbolData, k):
	g = mixture.GMM(n_components = k)
			
	g.fit(symbolData)
	bic = g.bic(symbolData)
	return bic
	
i = 0
def onEpochEnd(x, y):
	global min_err
	global test
	global ind_min_err
	global i

	i += 1
	if i % 100 == 0:
		x.save_params_to('autoEncoderChineModel' + '.npz')


def findOptK(symbolData, rangeOfK, prevLabel, f):
	symbolData = np.array(symbolData).astype(dtype = np.float32)
	symbolData = f(symbolData)
	func = partial(calcBic, symbolData)

	bics = p.map(func, rangeOfK)
	opt = rangeOfK[np.argmin(bics)]
	print('opt k: ', opt)

	with open('index' + str(hidden_size) + '.txt', 'a') as out:
		out.write(prevLabel + '\t' + str(opt) + '\n')


if __name__ == '__main__':
	finalAcc = []
	averageK = []
	
	hidden_size = int(sys.argv[1])
	chineAutoencoder = functional.initChineAutoencoder(hidden_size)

	testPathes = ['FeatureChineese/verify.txt']
	pathes = ['FeatureChineese/train.txt']
	

		
	chineAutoencoder.load_params_from('autoEncoderChineModel.npz')

	input_layer = chineAutoencoder.layers_['input'] 
	hidden_layer = chineAutoencoder.layers_['hidden']
	
	y = layers.get_output(hidden_layer)
	f = theano.function([input_layer.input_var], y)
	symbolData = []
	prevLabel = 'j_SHI'

	p = Pool(2)	

	for line in open(pathes[0]):
		line = line.rstrip('\n').split('\t')
	
		data = [int(el) for el in line[3:]]
		label = line[0]
		if label == prevLabel:
			symbolData.append(data)
		else:
			if len(symbolData) > 20:
				rangeOfK = range(1, 20)
			else:
				rangeOfK = range(1, len(symbolData))
			findOptK(symbolData, rangeOfK, prevLabel, f)
			prevLabel = label
			symbolData = []

	findOptK(symbolData, rangeOfK, prevLabel, f)
