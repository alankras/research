import numpy as np
from sklearn import mixture

import matplotlib.pyplot as plt
import os.path
from sklearn.decomposition import PCA, IncrementalPCA
from scipy.interpolate import interp1d

import scipy.fftpack
from multiprocessing import Pool
import time
from functools import partial
import functional

def updateRes(res, best, label):
	if best == -1:
		return np.array([[el, label] for el in res])
	else:
		for el, r in zip(best, res):
			if float(r) > float(el[0]):
				el[0] = r
				el[1] = label	
		return best

def predict(g, best, testData, label):
	start = time.time()
	res = g.score(testData)
	print('predicted time', time.time() - start)
	start = time.time()
	best = updateRes(res, best, label)
	print('up time', time.time() - start)
	return best
	

def trainGMM(symbolData, pca, labelInd):
	global optimals 

	symbolData = np.array(symbolData)
	symbolData = pca.transform(symbolData)
	
	optimalValue = int(optimals[labelInd][1])
	labelInd += 1

	g = mixture.GMM(n_components = optimalValue)
	g.fit(symbolData)

	best = predict(g, best, test, prevLabel)
	
	return best


if __name__ == '__main__':
	finalAcc = []
	averageK = []
	
	testPath = 'FeatureChineese/testPart.txt'
	trainPath = 'FeatureChineese/train.txt'

	hidden_size = int(sys.argv[1])

	optimals = functional.loadOptimalK('index' + str(hidden_size) + '.txt')
	
	pca = functional.incrementalPCA(pathes)
	
	symbolData = []
	prevLabel = 'j_SHI'


	test = functional.loadTestData(testPath)
	test = pca.transform(test)

	labelInd = 0 
	best = -1
	predictions = []
	for line in open(trainPath):
		line = line.rstrip('\n').split('\t')
	
		data = [int(el) for el in line[3:]]
		label = line[0]
		if label == prevLabel:
			symbolData.append(data)
		else:
			print('processing label ', label)
			best = trainGMM(symbolData, f, labelInd)
			labelInd += 1	
			prevLabel = label
			symbolData = []

	best = trainGMM(symbolData, f, labelInd)
	with open('prediction' + str(hidden_size) + '.txt', 'w') as out:
		for el in best:
			out.write(el[1] + '\n')

