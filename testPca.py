#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
import os.path
import sys

from sklearn import mixture
from sklearn.decomposition import PCA
from scipy.interpolate import interp1d

from functional import findOptimalK
from functional import plotBicCurve
from functional import makePrediction


#Считывание и сплит данных
def load(trainPath, testPath, testSize = 77565):
	allLabels = []
	allData = []

	for path in trainPath + testPath:
		content = [line.rstrip('\n').split('\t') for line in open(path)]
		label = [line[:1] for line in content]
		label = list(map(lambda x:x[0], label))
		content = [[int(el) for el in line[1:]] for line in content]
		allLabels += label
		allData += content

	testData = allData[testSize:]
	testLabels = allLabels[testSize:]
	allData = allData[:testSize]
	allLabels = allLabels[:testSize]

	return testData, testLabels, allData, allLabels	

if __name__ == '__main__':

	testPathes = ['Features-A/NP1000.A.txt']
	pathes = ['Features-A/NF100.A.txt', 'Features-A/NP125.A.txt', 'Features-A/NP250.A.txt', 'Features-A/NP500.A.txt']
	

	featureNumber = int(sys.argv[1])  # Размерность нового пространства (входной параметр скрипта)
	#featureNumber = 10
	testData, testLabels, allData, allLabels = load(pathes, testPathes)

	#применение pca
	pca = PCA(n_components = featureNumber) # 73 - default
	if featureNumber != 75:  # если 75 - обучение gmm на исходных данных
		allData = pca.fit_transform(allData)
		testData = pca.transform(testData)
	
	#маппинг данных
	mapLabels = {}
	for el, vector in zip(allLabels, allData):
		if el in mapLabels:
			mapLabels[el].append(vector)
		else:
			mapLabels[el] = [vector]

	optimal_k_map = findOptimalK(mapLabels)
	makePrediction(mapLabels, testData, featureNumber)
