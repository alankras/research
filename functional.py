#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import os.path
import sys

from sklearn import mixture
from sklearn.decomposition import PCA
from scipy.interpolate import interp1d

#Отрисовка графика bic в отдельную папку
def plotBicCurve(i, label, data, minK):
	fig = plt.figure()	

	for el in data:
		plt.plot(el[0], el[1])
	
	lab = 'optimal k for label:' + str(label) + ' is ' + str(minK[0]+1) +  '/' + str(minK[1] + 1) + '/' + str(minK[2] + 1) + '/' + str(minK[3] + 1)
	plt.title(lab)
	fig.savefig('smoothK/' + str(i) + '.png')
	plt.close(fig)

#Поиск оптимального числа кластеров в новом пространстве
def findOptimalK(mapLabels):
	optimal_k_map = {}
	allK = []

	for i, label in enumerate(mapLabels):	
		print('processing label:', label)
		res = []
		content = np.array(mapLabels[label])

		if len(content) > 50:
			rangeOfK = range(1, 50)
		else:
			rangeOfK = range(1, len(content))
		for k in rangeOfK:
			g = mixture.GMM(n_components = k)
			g.fit(content)
			bic = g.bic(content)
			res.append(bic)
		minK = res.index(min(res))
		
		if len(rangeOfK) < 45:
			allK.append(minK+1)
			optimal_k_map[label] = minK+1
		else: 
			#интерполяция графика bic (см. 3.1 дипломной работы)
			f = interp1d(rangeOfK, res, kind='cubic')
			xnew = np.linspace(1, 48, num=7, endpoint=True)
			xnew2 = np.linspace(10, 48, num=5, endpoint=True)
			xnew3 = np.linspace(1, 48, num=5, endpoint=True)	

			ynew = list(f(xnew))
			ynew2 = list(f(xnew2))
			ynew3 = list(f(xnew3))
			
			minK1 = xnew[ynew.index(min(ynew))]
			minK2 = xnew2[ynew2.index(min(ynew2))]
			minK3 = xnew3[ynew3.index(min(ynew3))]

			#plotBicCurve(i, label, [(rangeOfK, res),(xnew, ynew),(xnew2, ynew2),(xnew3, ynew3)], [minK, minK1, minK2, minK3])

			kk = int(min(minK, minK1, minK2, minK3)) + 1
			optimal_k_map[label] = kk
			allK.append(kk)
	print('Average K:', float(sum(allK)) / len(allK))	
	return optimal_k_map	

#prediction модели
def makePrediction(mapLabels, testData, featureNumber):
	predictions = []
	for label in mapLabels:
		print('Predicting label: ', label)
		content = mapLabels[label]
		content = np.array(content)

		g = mixture.GMM(n_components = optimal_k_map[label])
		g.fit(content)
		predictions.append((label, g.score(testData)))

	bestPred = predictions[:1][0][1]
	bestLabels = ['m']*len(bestPred)
	
	for pred in predictions[1:]:
		for i, el in enumerate(pred[1]):
			if el > bestPred[i]:
				bestPred[i] = el
				bestLabels[i] = pred[0]

	mistakeCount = 0
	for i, el in enumerate(bestLabels):
		if el != testLabels[i]:
			#сохранение всех ошибочных распознаваний в файл
			with open('mistakes' + str(featureNumber) + '.txt', 'a') as out:
				out.write('prediction: ' +  str(el) +  ' Real: ' + str(testLabels[i]) + '\n')
			mistakeCount += 1

	print('Accuracy:', 1 - float(mistakeCount) / len(testLabels))

#инициализация автоенкодера
def initAutoEncoder(hidden_size):
	autoEncoder = NeuralNet(
		layers=[
			('input', layers.InputLayer),
			('hidden', layers.DenseLayer),
			('output', layers.DenseLayer),
			],

		input_shape=(None, 73), 
		hidden_num_units=hidden_size, 
		output_nonlinearity=None,
		output_num_units=73,
		eval_size=0.0,

		update=adam,
		update_learning_rate=0.00005,
		#update_momentum=0.8,

		regression=True, 
		max_epochs=2000,  # we want to train this many epochs
		on_epoch_finished=[onEpochEnd],
		verbose=1,
		)
	return autoEncoder

def initNeural(hidden_size):
	neural = NeuralNet(
		layers=[
			('input', layers.InputLayer),
			('hidden', layers.DenseLayer),
			('output', layers.DenseLayer),
			],

		input_shape=(None, 73), 
		hidden_num_units=hidden_size, 
		output_nonlinearity=softmax,
		output_num_units=88,
		eval_size=0.0,

		update=adam,
		update_learning_rate=0.00015,
		#update_momentum=0.8,

		regression=False, 
		max_epochs=1200,  # we want to train this many epochs
		on_epoch_finished=[onEpochEnd],
		verbose=1,
		)
	return neural

def initChineAutoencoder(hidden_size):
	chineAutoencoder = NeuralNet(
		layers=[
			('input', layers.InputLayer),
	
			('hidden', layers.DenseLayer),
		
			('output', layers.DenseLayer),
			],

		input_shape=(None, 269), 
		hidden_num_units=hidden_size, 
		output_nonlinearity=None,
		output_num_units=269,
		eval_size=0.0,

		update=adam,
		update_learning_rate=0.000012,
		#update_momentum=0.8,

		regression=True, 
		max_epochs=1200,  # we want to train this many epochs
		on_epoch_finished=[onEpochEnd],
		verbose=1,
		)
	return chineAutoencoder

#нормировка выборки
def normalize(train, test):
	mean = train.mean(axis = 0)
	std = train.std(axis = 0)
	train = train - mean
	train = train / std
	mean = test.mean(axis = 0)
	std = test.std(axis = 0)
	test = test - mean
	test = test / std 	

	return train, test

#обучение pca итерационно
def incrementalPCA(path, batch = 200000):
	pca = IncrementalPCA(n_components = hidden_size, batch_size=batch) # 269 - default
	i = 0
	
	for path in pathes:
		content = []
		for line in open(path):
			i += 1
			content.append(line.rstrip('\n').split('\t'))
			if i % batch == 0:
				print('in process ' + str(i))
				content = [[int(el) for el in line[3:]] for line in content]
				pca = pca.partial_fit(content)
				content = []
		content = [[int(el) for el in line[3:]] for line in content]
		pca = pca.partial_fit(content)
		content = []
	return pca

#загрузка оптимальных k для китайского датасета
def loadOptimalK(path):
	optimals = []
	with open(path, 'r') as inFile:
		optimals = inFile.readlines()
	optimals = [line.rstrip().split('\t') for line in optimals]

	return optimals

def loadTestData(testPath):
	test = []

	for line in open(testPath):
		test.append(line.rstrip('\n').split('\t'))
	
	test = [[int(el) for el in line[3:]] for line in test]
	test = np.array(test).astype(dtype=np.float32)
	
	return test