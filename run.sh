#Examples of usage

#pca on european data
python testPca 10

#encoder on europen
trainAutoencoder.py 10
testAutoencoder.py 10

#neural on european
trainNeural.py 10
testNeural.py 10

#encoder on chineese data
python chineEncoder.py 100
python chineIndexes.py 100
python testChineEncoder.py 100
python accuracy.py 100

#pca on chineese data
chineesePCA.py 100
testChineesePCA.py 100
python accuracy.py 100

#plotting results
python plotRes.py
